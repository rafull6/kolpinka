<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google-site-verification" content="XHXuUdBsk7a5RoPvDafjNhcl5c4U8bmdewMx11PCLyw" />
	<meta name="yandex-verification" content="8448a4b732c197a7" />
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	<link rel="stylesheet" href="https://cdn.callbackkiller.com/widget/cbk.css">
</head>

<body <?php body_class(); ?>>
	    <header>
	        <div class="container_16">
	            <div class="row">
	                <div class="grid_16">
	                    <a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="Колпинка" class="logo"></a>
	                    <nav>
							<?php wp_nav_menu( array( 'theme_location' => '', 'menu_class' => 'sf-menu') ); ?>
	                    </nav>
	                </div>
	            </div>
	        </div>
	        <div class="bg-1">
	            <div class="bg-3-custom">
	            	<div class="container">
						<h1 class="header header-2"><?php the_title(); ?>
							<?php if ( current_user_can( 'manage_options' ) ) : ?>
								<div class="edit-link h-pos">
									<?php edit_post_link('<img src="' .  get_bloginfo('template_directory') . '/images/edit.png" />'); ?>
								</div>
							<?php endif; ?>
						</h1>

					</div>
	            </div>
	        </div>
	    </header>
	    <div class="content">
