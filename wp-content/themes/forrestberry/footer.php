</div>
    <footer>
        <div class="bg-3">
            <div class="container_16 bord-3">
                <div class="row main-foot">
                    <div class="container_16">
                        <div class="grid_4 bord-1">
                          <!-- socials -->
                          <ul class="list-services clearfix">
                           </ul>
                        </div>
                        <div class="grid_6 bord-1 prefix_1">
                          <div class="footer-adr">
                            <?php
                              $post_id = 92;
                              $queried_post = get_post($post_id);
                            ?>
                          <div class="icon-1">
                            <?php echo $queried_post->post_content; ?>
                          </div>
						</div>
                          <?php wp_reset_postdata() ?>
                        </div>
                        <div class="grid_4 prefix_1">

                            <div class="footer-con">
                                <?php
                                $emailField = get_field( "e-mail", 107 );
                                $phoneField = get_field( "phone", 107 );
                                ?>

                              <div class="icon-2">
                                  <?php if(!empty($emailField)) : ?>
                                    <a href="mailto:<?php echo $emailField ?>" target="_top"><?php echo $emailField; ?></a>
                                  <?php endif; ?>
                              </div>
                                <div class="icon-3">
                                      <?php if(!empty($phoneField)) : ?>
                                          <a href="tel:<?php echo $phoneField; ?>"><?php echo $phoneField; ?></a>
                                      <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-4">
            <div class="container_16">
                <div class="row grid_16">
                    <div class="inside">
                        <a href="index.html">
                          <img src="<?php bloginfo('template_directory'); ?>/images/footer-logo.png" alt=""></a>
                        <br/>
                        <span>&copy; 1922 &#8226; Колпинка</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php wp_footer(); ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter38306780 = new Ya.Metrika({
                    id:38306780,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/38306780" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script type="text/javascript" src="https://cdn.callbackkiller.com/widget/cbk.js?wcb_code=f9b42908f9fcccc4c30b6cbd8f7bdf08" charset="UTF-8" async></script>
<script type='text/javascript'>
    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        (function () {
            var widget_id = 'xpFFAwlOpN';
            var d = document;
            var w = window;

            function l() {
                var s = document.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = '//code.jivosite.com/script/widget/' + widget_id;
                var ss = document.getElementsByTagName('script')[0];
                ss.parentNode.insertBefore(s, ss);
            }

            if (d.readyState == 'complete') {
                l();
            } else {
                if (w.attachEvent) {
                    w.attachEvent('onload', l);
                } else {
                    w.addEventListener('load', l, false);
                }
            }
        })();
    }
</script>
</body>
</html>
