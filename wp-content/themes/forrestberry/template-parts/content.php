<li class="half-post">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<a href="<?php echo esc_url( get_permalink() ); ?>" class="archive-header">
			<h2 class="archive-title tstyle brown"><?php the_title(); ?></h2>

		</a>

		<a href="<?php echo esc_url( get_permalink() ); ?>"  class="archive-content">
			<?php
				if ( has_post_thumbnail() ) {
			?>
				<div class="img-holder"><img class="archive-image" src="<?php the_post_thumbnail_url( 'medium' ); ?>" /></div>
				<?php } ?>
			<div class="text-content">
			<?php
				$content = get_the_content();
				 echo wp_trim_words( $content , '50' );
			?>
			</div>
		</a>

		<div class="post-info marger-b-40">
			<span class="archive-date-margin">Дата добавления: </span><?php the_date(); ?>
			<?php if ( current_user_can( 'manage_options' ) ) : ?>
				<div class="edit-link b-pos">
					<?php edit_post_link('<img src="' . get_bloginfo('template_directory') . '/images/edit.png" />'); ?>
				</div>
			<?php endif; ?>
		</div>
	</article>
</li>
