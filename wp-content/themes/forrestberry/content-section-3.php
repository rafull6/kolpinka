<div class="bord-2">
    <div class="color-1">
        <a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a>
    </div>
    <div>
      <?php
        $content = get_the_content();
        echo wp_trim_words( $content , '10' ); 
       ?>
    </div>
</div>
