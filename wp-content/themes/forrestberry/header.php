<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="XHXuUdBsk7a5RoPvDafjNhcl5c4U8bmdewMx11PCLyw"/>
    <meta name="yandex-verification" content="8448a4b732c197a7"/>
    <meta name="theme-color" content="#2E1905">
    <meta name="msapplication-navbutton-color" content="#2E1905">
    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>
    <?php wp_head(); ?>
    <script>
        jQuery(window).load(function ($) {
            jQuery('#camera_wrap_1').camera({
                height: '73,00%',
                // thumbnails: true,
                playPause: false,
                time: 8000,
                transPeriod: 900,
                fx: 'simpleFade',
                loader: 'none',
                minHeight: '200px',
                navigation: false
            });
            jQuery('#foo').carouFredSel({
                auto: false,
                responsive: true,
                width: '100%',
                prev: '#prev1',
                next: '#next1',
                scroll: 1,
                items: {
                    height: 'auto',
                    width: 300,
                    visible: {
                        min: 1,
                        max: 1
                    }
                },
                mousewheel: false,
                swipe: {
                    onMouse: true,
                    onTouch: true
                }
            });

        });
    </script>
    <link rel="stylesheet" href="https://cdn.callbackkiller.com/widget/cbk.css">
</head>

<body <?php body_class(); ?>>
<header>
    <div class="container_16">
        <div class="row">
            <div class="grid_16">
                <a href="<?php echo get_home_url(); ?>">
                    <img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="Колпинка" class="logo"></a>
                <nav>
                    <div class="nav-desktop">
                        <?php wp_nav_menu(array('menu_class' => 'sf-menu')); ?>


                    </div>
                </nav>
                <ul class="cd-accordion-menu animated">
                    <li class="has-children">
                        <input  type="checkbox" name ="group-1" id="group-1">
                        <label id="show" for="group-1" class="icon">Меню сайта</label>
                        <?php wp_nav_menu(array('menu_class' => 'child-keeper',
                    'container_id' => 'menu-mobile', 'depth' => '2')); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </div>
    <div class="bg-1">
        <div class="bg-2">
            <div class="container_16">
                <div class="row">
                    <div class="grid_16 overflowa clearfix">
                        <div class="headerslider">
                            <?php if (function_exists('meteor_slideshow')) {
                                meteor_slideshow();
                            } ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
                <div class="shadow"></div>
                <div class="row">
                    <div class="grid_16">
                        <?php
                        $subtitle_id = 161;
                        $queried_post = get_post($subtitle_id);
                        ?>
                        <div class="title-1">
                            <?php echo $queried_post->post_content; ?>
                        </div>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="list_carousel responsive clearfix carousel-1">
                        <ul id="foo" class="clearfix">
                            <?php
                            $args = array('cat' => 5);
                            $content_section_2_posts = new WP_Query($args);
                            if ($content_section_2_posts->have_posts()) {
                                while ($content_section_2_posts->have_posts()) : $content_section_2_posts->the_post(); ?>
                                    <li class="grid_16">
                                        <div class="text-1">
                                            <?php the_content(); ?>
                                        </div>
                                    </li>
                                    <?php
                                endwhile;
                            }
                            wp_reset_postdata();
                            ?>
                        </ul>
                        <div class="arrows">
                            <a id="prev1" class="prev" href="#"></a>
                            <a id="next1" class="next" href="#"></a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="content">
