<?php

if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
    require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'forestberry_setup' ) ) :
    function forestberry_setup() {
        load_theme_textdomain( 'forestberry', get_template_directory() . '/languages' );
        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'title-tag' );
        add_theme_support( 'custom-logo', array(
            'height'      => 240,
            'width'       => 240,
            'flex-height' => true,
        ) );
        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 1200, 9999 );
        register_nav_menus( array(
            'primary' => __( 'Primary Menu', 'forestberry' ),
            'social'  => __( 'Social Links Menu', 'forestberry' ),
        ) );
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );
        add_theme_support( 'post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'status',
            'audio',
            'chat',
        ) );
        add_editor_style( array( 'css/editor-style.css', forestberry_fonts_url() ) );
        add_theme_support( 'customize-selective-refresh-widgets' );
    }
endif; // forestberry_setup
add_action( 'after_setup_theme', 'forestberry_setup' );

function forestberry_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'forestberry_content_width', 840 );
}
add_action( 'after_setup_theme', 'forestberry_content_width', 0 );

function forestberry_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Sidebar', 'forestberry' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'forestberry' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Content Bottom 1', 'forestberry' ),
        'id'            => 'sidebar-2',
        'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'forestberry' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Content Bottom 2', 'forestberry' ),
        'id'            => 'sidebar-3',
        'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'forestberry' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Правая боковая панель' ),
        'id' => 'rightbar',
        'description' => __( 'Боковая панель на странице записей.' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>'
    ) );

}
add_action( 'widgets_init', 'forestberry_widgets_init' );


if ( ! function_exists( 'forestberry_fonts_url' ) ) :
    function forestberry_fonts_url() {
        $fonts_url = '';
        $fonts     = array();
        $subsets   = 'latin,latin-ext';

        if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'forestberry' ) ) {
            $fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
        }

        if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'forestberry' ) ) {
            $fonts[] = 'Montserrat:400,700';
        }

        if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'forestberry' ) ) {
            $fonts[] = 'Inconsolata:400';
        }

        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
                'subset' => urlencode( $subsets ),
            ), 'https://fonts.googleapis.com/css' );
        }

        return $fonts_url;
    }
endif;

function forestberry_javascript_detection() {
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'forestberry_javascript_detection', 0 );

function forestberry_scripts() {

    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'camera', get_template_directory_uri() . '/css/camera.css');
    wp_enqueue_style( 'skeleton', get_template_directory_uri() . '/css/skeleton.css');
    wp_enqueue_style( 'superfish', get_template_directory_uri() . '/css/superfish.css');

    //TODO vova kaban
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.js', array(), false, false );
    wp_enqueue_script( 'jquery.carouFredSel', get_template_directory_uri() . '/js/jquery.carouFredSel-6.1.0-packed.js', array(), false, false );
    wp_enqueue_script( 'jquery.easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array(), false, false );
    wp_enqueue_script( 'jquery.mobile.customized.min', get_template_directory_uri() . '/js/jquery.mobile.customized.min.js', array(), false, false );
    wp_enqueue_script( 'jquery.mobilemenu', get_template_directory_uri() . '/js/jquery.mobilemenu.js', array(), false, false );
    wp_enqueue_script( 'jquery.mousewheel.min', get_template_directory_uri() . '/js/jquery.mousewheel.min.js', array(), false, false );
    wp_enqueue_script( 'jquery.touchSwipe.min', get_template_directory_uri() . '/js/jquery.touchSwipe.min.js', array(), false, false );
    wp_enqueue_script( 'camera', get_template_directory_uri() . '/js/camera.js', array(), false, false );
    wp_enqueue_script( 'superfish', get_template_directory_uri() . '/js/superfish.js', array(), false, false );
    wp_enqueue_script( 'accordion-menu', get_template_directory_uri() . '/js/accordion-menu.js', array(), false, false );


    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    if ( is_singular() && wp_attachment_is_image() ) {
        wp_enqueue_script( 'forestberry-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160412' );
    }

    wp_localize_script( 'forestberry-script', 'screenReaderText', array(
        'expand'   => __( 'expand child menu', 'forestberry' ),
        'collapse' => __( 'collapse child menu', 'forestberry' ),
    ) );
}
add_action( 'wp_enqueue_scripts', 'forestberry_scripts' );

function forestberry_body_classes( $classes ) {
    if ( get_background_image() ) {
        $classes[] = 'custom-background-image';
    }

    if ( is_multi_author() ) {
        $classes[] = 'group-blog';
    }

    if ( ! is_active_sidebar( 'sidebar-1' ) ) {
        $classes[] = 'no-sidebar';
    }

    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }
    return $classes;
}
add_filter( 'body_class', 'forestberry_body_classes' );

function forestberry_hex2rgb( $color ) {
    $color = trim( $color, '#' );

    if ( strlen( $color ) === 3 ) {
        $r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
        $g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
        $b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
    } else if ( strlen( $color ) === 6 ) {
        $r = hexdec( substr( $color, 0, 2 ) );
        $g = hexdec( substr( $color, 2, 2 ) );
        $b = hexdec( substr( $color, 4, 2 ) );
    } else {
        return array();
    }

    return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

require get_template_directory() . '/inc/template-tags.php';

require get_template_directory() . '/inc/customizer.php';

function forestberry_content_image_sizes_attr( $sizes, $size ) {
    $width = $size[0];

    840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

    if ( 'page' === get_post_type() ) {
        840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
    } else {
        840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
        600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
    }

    return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'forestberry_content_image_sizes_attr', 10 , 2 );

function forestberry_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
    if ( 'post-thumbnail' === $size ) {
        is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
        ! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
    }
    return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'forestberry_post_thumbnail_sizes_attr', 10 , 3 );

function forestberry_widget_tag_cloud_args( $args ) {
    $args['largest'] = 1;
    $args['smallest'] = 1;
    $args['unit'] = 'em';
    return $args;
}
add_filter( 'widget_tag_cloud_args', 'forestberry_widget_tag_cloud_args' );


function home_page_menu_args( $args ) {
    $args['show_home'] = true;
    return $args;
}
add_filter( 'wp_page_menu_args', 'home_page_menu_args' );

function content_section_one() {
    $args = array('cat' => 6);
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts()) {
        while ( $the_query->have_posts()) {
            $the_query->the_post();
            if ( has_post_thumbnail() ) {
                $string .= the_post_thumbnail();
            };
            $string .= the_title();
            $string .= the_content();
        }
    }
    return $string;
    wp_reset_postdata();
}

function new_excerpt_length($length) {
    return 10;
}
add_filter('excerpt_length', 'new_excerpt_length');

function new_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');



add_filter( 'get_the_archive_title', function ($title) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>' ;
    }
    return $title;
});

add_image_size( 'mycustomsize', 260, 190, true );

// Call to action buttons //
class cta_widget extends WP_Widget {

    public function __construct() {
        parent::__construct(
            'kolpinka_call_to_action',
            'Кпопки боковой панели Kolpinka.ru',
            array( 'description' => __( 'Виджет кнопок на странице записи', 'text_domain' ), )
        );
    }

    public function widget( $args, $instance ) {
        echo '<div class="buttons-wrapper">';
        echo '<a href="http://kolpinka.ru/kontakty/" class="action_button_orange">СДЕЛАТЬ ЗАКАЗ</a>';
        echo '<a href="http://kolpinka.ru/usloviya-raboty/" class="action_button_green">ДОСТАВКА И ОПЛАТА</a>';
        echo '</div>';
    }
}

add_action( 'widgets_init', function(){
    register_widget( 'cta_widget' );
});

// Similar posts widget (same category) //
class category_posts_widget extends WP_Widget {

    public function __construct() {
        parent::__construct(
            'kolpinka_category_posts',
            'Записи категории',
            array( 'description' => __( 'Виджет отображающий записи с категории', 'text_domain' ), )
        );
    }

    public function widget( $args, $instance ){
        echo '<div class="similar-posts"><h1>Интересные записи</h1>';
        global $post;
        $cat_ID=array();
        $categories = get_the_category();
        foreach($categories as $category) {
            array_push($cat_ID,$category->cat_ID);
        }
        $args = array(
            'orderby' => 'date',
            'order' => 'DESC',
            'post_type' => 'post',
            'numberposts' => 5,
            'post__not_in' => array($post->ID),
            'category__in' => $cat_ID
        );
        $cat_posts = get_posts($args);
        if ($cat_posts) {
            foreach ($cat_posts as $cat_post) {
                echo '<div class="post">';
                echo '<h1><a href="' . get_permalink($cat_post->ID) . '">' . $cat_post->post_title . '</a></h1>';
                echo wp_trim_words( $cat_post->post_content , '15' );
                echo '</div>';
            }
        }
        echo '</div>';
    }
}

add_action( 'widgets_init', function(){
    register_widget( 'category_posts_widget' );
});
