<?php get_header("custom"); ?>
<div class="bg-1">
	<div class="post-container">
		<div class="not-found"><span>Страница не найдена</span></div><br />
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" ><span style="display:block; padding: 10px; text-align: center;">На главную страницу</span><br /></a>
	</div>
</div>
<?php get_footer(); ?>