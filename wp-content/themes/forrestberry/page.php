<?php get_header("custom"); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		while ( have_posts() ) : the_post(); ?>
		<article>
			<div class="bg-1 bg-1-white">
				<div class="entry-container">
					<div class="entry-content">
						<?php
							$location = get_field('map-adr');
						if( !empty($location) ):
						?>
						<div class="acf-map">
							<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
						</div>
						<?php endif; ?>
						<div class="container"><?php the_content(); ?></div>
					</div>
					<?php if(is_page(array(contacts))){
						echo '<div class="entry-info" style="display: none">';
					}else{
						echo '<div class="entry-info">';
					}?>
						<span>Дата: </span><?php the_date(); ?>
					</div>
				</div>
			</div>
		</article>
		<?php endwhile; ?>
	</main><!-- .site-main -->
</div><!-- .content-area -->
<div style="clear: both"></div>
<?php get_footer(); ?>
