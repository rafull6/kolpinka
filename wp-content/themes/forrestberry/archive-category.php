<div class="grid_4 prefix_1 bord-1 btn-wrapper">
    <div class="maxheight">
        <div class="aligncenter">
        <?php
			$image = get_field('index-img-round');
        	if ( !empty($image) ) {
               echo '<div class="round-img-border"><div class="round-img"><img src="';
               echo $image['url'];
               echo '"/></div></div>';
          	}else{
          	}
	    ?>
        </div>
        <h2 class="tstyle"><?php the_title(); ?></h2>
        <div class="separator">
            <img src="<?php bloginfo('template_directory'); ?>/images/separator.png" alt="" />
        </div>
        <div class="preview-content">
            <p>
            <?php
                $content = get_field('short-desc');
                echo wp_trim_words( $content , '30' );
            ?>
            </p>
        </div>
        <a class="btn" href="<?php echo esc_url( get_permalink() ); ?>"><span>Поробнее</span></a>
    </div>
</div>
