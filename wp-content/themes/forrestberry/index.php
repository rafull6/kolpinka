<?php get_header(); ?>
<div class="bg-1">
  <div class="container_16">
      <div class="row row-1">
        <?php
           $args = array('cat' => 6, 'posts_per_page' => 3);
           $content_section_1_posts = new WP_Query($args);
           if($content_section_1_posts->have_posts()){
              while($content_section_1_posts->have_posts()){
                 $content_section_1_posts->the_post();
                 get_template_part('content-section-1');
               }
             }
          ?>
      </div>
  </div>
</div>
<div class="container_16">
    <div class="row">
      <?php
         $args = array('cat' => 7, 'posts_per_page' => 2);
         $content_section_2_posts = new WP_Query($args);
         if($content_section_2_posts->have_posts()){
            while($content_section_2_posts->have_posts()){
               $content_section_2_posts->the_post();
               get_template_part('content-section-2');
             }
           }
        ?>
        <div class="grid_4 p1">
            <h2>Условия работы</h2>
            <div class="num-lists">
              <?php
                 $args = array('cat' => 8, 'posts_per_page' => 3);
                 $content_section_2_posts = new WP_Query($args);
                 if($content_section_2_posts->have_posts()){
                    while($content_section_2_posts->have_posts()){
                       $content_section_2_posts->the_post();
                       get_template_part('content-section-3');
                     }
                   }
                ?>
            </div>
            <?php $category_link = get_category_link( 8 ); ?>
		<!--<a href="<?php /*echo esc_url($category_link); */?>" class="btn-2 inverted">Больше новостей</a>-->
        </div>
    </div>
</div>
<?php get_footer(); ?>
