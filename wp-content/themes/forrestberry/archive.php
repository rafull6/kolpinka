<?php get_header("category"); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="archive-container">
			<?php if ( have_posts() ) : ?>
			<ul class="archive-post-container">
				<?php
				$cat = get_query_var('cat');
				global $ancestor;

				if($cat==14 || $cat==13){ // check if current post in specialized category
					$childcats = get_categories('child_of=' . $cat . '&hide_empty=1'); //get the child categories of current category
					foreach ($childcats as $childcat) { //for every child category from array of child categories do
						if (cat_is_ancestor_of($ancestor, $childcat->cat_ID) == false) { //if loop category is ancestor
							echo '<div class="cat-filter-holder"><a class="title-link" href="'.get_category_link($childcat->cat_ID).'">' . $childcat->cat_name . '</a>';
							$subCategory = new WP_Query( 'cat=' . $childcat->cat_ID);
							echo '<div class="content">';
							while ($subCategory->have_posts()) {
								$subCategory->the_post();
								//echo the_title() . '<br />';
								if(!in_category(27)) { //if post is multifunctional it will not be displayed
									get_template_part('template-parts/content-special', get_post_format());
								}
							}
							echo '</div></div>';
							$ancestor = $childcat->cat_ID;
						}
					}
				}else{
					while(have_posts()){
						the_post();
						get_template_part( 'template-parts/content', get_post_format() );
					}
				}
				?>

				<?php
				if($cat==14 || $cat==13){
					echo '<ul class="navigation-wrapper" style="display: none">';
				}else{
					echo '<ul class="navigation-wrapper">';
				}?>
				<?php
					the_posts_pagination( array(
						'prev_text'          => __( '<' ),
						'next_text'          => __( '>' ),
						'before_page_number' => '',
					) );

					else :
						get_template_part( 'template-parts/content', 'none' );
					endif;
					?>
				</ul>
			</ul>
			<div style="clear: both"></div>
		</div>
	</main>
</div>

<?php get_footer(); ?>
