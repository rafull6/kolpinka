<div class="grid_5 suffix_1">
    <div class="wrapper p1">
      <?php  if ( has_post_thumbnail() ) {
		$image_src = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		$post_thumbnail_image = '<img src="' . $image_src . '" />';
		echo $post_thumbnail_image;
    } ?>
  </div>
    <div class="title-2"><?php the_title(); ?></div>
    <div>
      <?php
        $content = get_the_content();
        echo wp_trim_words( $content , '50' ); 
      ?>
    </div>
    <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn-2">Поробнее</a>
</div>
