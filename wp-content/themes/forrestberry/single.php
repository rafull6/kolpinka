<?php get_header("custom"); ?>
    <div class="bg-1">
        <div class="post-page-container">
            <div class="post-container">
                <?php
                $category = get_the_category();
                $arr_size = count($category);
                $test_array = null;

                for ($i = 0; $i < $arr_size; $i++){
                    $mainCategory = $category[$i]->cat_ID;
                    if($mainCategory != 27 && $mainCategory != 6){
                        $test_array = array($mainCategory);
                    }
                }

                $postId = get_the_ID();
                ?>
                <?php if ( in_category(27) ) : ?>
                    <?php
                    query_posts(array('cat' => $test_array, 'post__not_in' => array($postId), 'posts_per_page' => 3, 'category__not_in' => array(6)));
                    ?>
                    <div class="container">
                        <ul class="list">
                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php
                                $subPostId = get_the_ID();
                                $categories = wp_get_post_categories($subPostId);
                                $category_check = array_search(6, $categories);
                                $cat_exclude = array(27);

                                $result = array_diff($categories, $cat_exclude);
                                ?>
                                <?php if($category_check !== true) : ?>
                                    <li>
                                        <?php
                                            $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' );
                                        ?>
                                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="maxheight singleheight">
                                            <div class="round-img-border thinborder">
                                                <div class="round-img">
                                                    <img src="<?php echo $url ?>"/>
                                                </div>
                                            </div>
                                            <h2 class="tstyle" style="margin-top: 20px;"><?php the_title(); ?></h2>
                                            <div class="separator">
                                                <img class="img-single-page" src="<?php bloginfo('template_directory'); ?>/images/separator.png" alt=""/>
                                            </div>
                                            <div class="preview-content">
                                                <p>
                                                    <?php
                                                    $subContent = get_field('short-desc', $post->ID);
                                                    echo wp_trim_words( $subContent , '30' );
                                                    ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php if ( in_category(27) ) : ?>
                        <div class="post-content nobg">
                            <div class="separ">
                                <img src="<?php bloginfo('template_directory'); ?>/images/separator-main.png" />
                            </div>
                            <div class="container morepad"><?php the_content(); ?></div>
                        </div>
                    <?php elseif ( !in_category(27) ) : ?>
                        <div class="post-content">
                            <div class="container"><?php the_content(); ?></div>
                        </div>
                        <div class="post-info">
                            <span>Дата добавления: </span><?php the_date(); ?>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
            </div>
            <?php if ( in_category(array(27,1,12,5,7,9,23,8,6))) : ?>
                <div id="rightbar" class="post-sidebar hidden">
                    <?php dynamic_sidebar( 'rightbar' ); ?>
                </div>

            <?php elseif ( !in_category(27) ) : ?>

                <div id="rightbar" class="post-sidebar">
                    <?php dynamic_sidebar( 'rightbar' ); ?>
                </div>

            <?php endif; ?>
        </div>
    </div>
<?php get_footer(); ?>