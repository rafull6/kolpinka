<?php
/*
Plugin Name: Test plugin
Description: A test plugin to demonstrate wordpress functionality
Author: Simon Lissack
Version: 0.1
*/

function add_categories_controll_to_menu() {
  	add_posts_page( 'Управление рубриками', 'Управление рубриками', 'manage_options', 'ccontrol-plugin', 'ccats_init' );
}
add_action('admin_menu', 'add_categories_controll_to_menu');

function ccats_init(){
	$result ='<div class="wrap"><h2 style="padding:15px 0">Управление рубриками</h2></div>';

	echo $result;

	$args = array(
	  'orderby' => 'name',
	  'orderby' => 'name',
	  'parent' => 0
	  );
	  
	$categories = get_categories( $args );

	
	foreach ( $categories as $category ) {
		$args = array(
		  'cat' => $category->term_id,
		);
		$the_query = new WP_Query( $args );
	    echo '<div class="medium-4 wpcolumns"><div class="btn-design"><a href="http://kolpinka.ru/wp-admin/edit.php?cat=' . $category->term_id . '">' . $category->name . '<div class="counter">' . $the_query->found_posts . '</div></a></div></div>';
	}
}

function load_custom_wp_admin_style() {
        wp_register_style( 'style-cc', plugin_dir_url(__FILE__) . 'style-cc.css', false, '1.0.0' );
        wp_enqueue_style( 'style-cc' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

?>