<?php

class gmap_widget extends WP_Widget {

    public $base_id = 'gmap_widget'; 
    public $widget_name = 'Kolpinka Google Map'; 
    public $widget_options = array(
        'description' => 'Kolpinka Google Map'
    );

    public function __construct() {
        parent::__construct($this->base_id, $this->widget_name, $this->widget_options);
    }

    public function widget($args, $instance) {
        extract($args);
        extract($instance);

        $gmap_title = $title == '' ? get_option('gmap_title') : $title;
        $gmap_lat = $latitude == '' ? get_option('gmap_lat') : $latitude;
        $gmap_long = trim($longitude) == '' ? get_option('gmap_long') : $longitude;
        $gmap_width = $width == '' ? get_option('gmap_width') : $width;
        $gmap_height = $height == '' ? get_option('gmap_height') : $height;
        $gmap_zoom = $zoom == '' ? get_option('gmap_zoom') : $zoom;
        $gmap_type = $map_type;

        echo $before_widget
        ?>
        <script type="text/javascript">
            function initMap() {
                var mapDiv = document.getElementById("gmp_<?php echo $this->id; ?>");
                var map = new google.maps.Map(mapDiv, {
                    center: new google.maps.LatLng(<?php echo $gmap_lat; ?>,<?php echo $gmap_long; ?>),
                    zoom:<?php echo $gmap_zoom; ?>,
                    mapTypeId: google.maps.MapTypeId.<?php echo $gmap_type; ?>
                });
				 var marker = new google.maps.Marker({
					position: new google.maps.LatLng(<?php echo $gmap_lat; ?>,<?php echo $gmap_long; ?>),
					map: map,
				});
              }
            google.maps.event.addDomListener(window, 'load', initMap);
        </script>

        <div id="gmp_<?php echo $this->id; ?>" style="width:<?php echo $gmap_width . '%'; ?>;height:<?php echo $gmap_height . 'px'; ?>;"></div>
        <?php
        echo $widget_body_text
        . $after_widget;
    }

    public function form($instance) {
        ?>  
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Название карты (произвольное):</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" type="text" class="widefat">
        </p> 

        <p>
            <label for="<?php echo $this->get_field_id('latitude'); ?>">Широта (координаты):</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('latitude'); ?>" name="<?php echo $this->get_field_name('latitude'); ?>" value="<?php echo $instance['latitude']; ?>" type="text" class="widefat">
        </p>  

        <p>
            <label for="<?php echo $this->get_field_id('longitude'); ?>">Долгота (координаты):</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('longitude'); ?>" name="<?php echo $this->get_field_name('longitude'); ?>" value="<?php echo $instance['longitude']; ?>" type="text" class="widefat">
        </p>        

        <p>
            <label for="<?php echo $this->get_field_id('width'); ?>">Ширина в %:</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>" value="<?php echo $instance['width']; ?>" type="text" class="widefat">
        </p>        

        <p>
            <label for="<?php echo $this->get_field_id('height'); ?>">Высота в пикселях (по умолчанию 200):</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('height'); ?>" name="<?php echo $this->get_field_name('height'); ?>" value="<?php echo $instance['height']; ?>" type="text" class="widefat">
        </p>        

        <p>
            <label for="<?php echo $this->get_field_id('zoom'); ?>">Уровень приближения (по умолчанию 16):</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('zoom'); ?>" name="<?php echo $this->get_field_name('zoom'); ?>" value="<?php echo $instance['zoom']; ?>" type="text" class="widefat">
        </p>        

        <p>
            <label for="<?php echo $this->get_field_id('map_type'); ?>">Тип карты (по умолчанию "дороги"):</label>
        </p>
        <p>
            <select  name="<?php echo $this->get_field_name('map_type'); ?>" id="<?php echo $this->get_field_id('map_type'); ?>" class="widefat">
                <option <?php selected($instance['map_type'], 'ROADMAP'); ?>>Дороги</option>
                <option <?php selected($instance['map_type'], 'SATELLITE'); ?>>Спутник</option>
                <option <?php selected($instance['map_type'], 'HYBRID'); ?>>Гибрид</option>
                <option <?php selected($instance['map_type'], 'TERRAIN'); ?>>Ландшафт</option>
            </select>
        </p>        
        <?php
    }

}

add_action('widgets_init', create_function('', 'return register_widget("gmap_widget");'));
