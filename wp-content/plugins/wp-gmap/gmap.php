<?php
/*
  Plugin Name: Google Map
  Plugin URI: http://www.srmilon.com
  Description: The plugin will help to embed google map in post and pages also in sidebar.
  Author: SRMILON
  Author URI: http://www.srmilon.com
  Version: 1.2.5
 */

if (!defined('ABSPATH')) {
    exit("Your don't have required permission.");
}

if (!class_exists('gmap_main')):

    class gmap_main {

        private $plugin_name = 'Google Map';

        function __construct() {
            add_action('wp_enqueue_scripts', array($this, 'gmap_enqueue_scripts'));
            add_action('admin_init', array($this, 'gmap_register_fields'));
        }

        public function gmap_enqueue_scripts() {
            wp_enqueue_script('gmap_api', 'http://maps.googleapis.com/maps/api/js?key=AIzaSyA6QSKW1MfOfIQFRbxeXA6QU4IEtfSv3Sw', array('jquery'));
        }

        public function gmap_register_fields() {
            register_setting('gmap_settings_group', 'gmap_title');
            register_setting('gmap_settings_group', 'gmap_lat');
            register_setting('gmap_settings_group', 'gmap_long');
            register_setting('gmap_settings_group', 'gmap_width');
            register_setting('gmap_settings_group', 'gmap_height');
            register_setting('gmap_settings_group', 'gmap_zoom');
            register_setting('gmap_settings_group', 'gmap_type');
        }
    }
    endif;

new gmap_main();
require_once plugin_dir_path(__FILE__) . '/includes/widget.php';
?>
