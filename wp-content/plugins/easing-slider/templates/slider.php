<?php easingslider_inline_styles($slider); ?>
<?php easingslider_inline_script($slider); ?>

<div data-id="<?php echo esc_attr($slider->ID); ?>" <?php easingslider_container_classes($slider); ?>>
	<?php foreach ($slider->slides as $slide) : ?>
		<div <?php easingslider_slide_classes($slide, $slider); ?>>
			<?php easingslider_display_slide($slide, $slider); ?>
			<div class="slider-content">
				<?php
					echo $slider->slide;
					$alt = get_post_meta($slide->ID, '_wp_attachment_image_alt', true);
					$image_title = $attachment->post_title;
					$caption = $attachment->post_excerpt;
					$description = $image->post_content;
					echo $image_title . ',' . $caption . ',' . $description;
				?>
			</div>
		</div>
	<?php endforeach; ?>
</div>
