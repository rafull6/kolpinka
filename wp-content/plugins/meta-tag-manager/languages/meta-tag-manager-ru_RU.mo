��    	      d      �       �   {   �      ]  �   i     �     �               #  �  *  ?   �  "   �  �     3   �       $   ,     Q     o                	                              A simple plugin to manage meta tags that appear on all your pages. This can be used for verifiying google, yahoo, and more. Add new tag Enter a reference name (something to help you remember the meta tag) and then enter the values that you want the meta tag to hold. Homepage only Meta Tag Meta Tag Manager Reference Name Remove Project-Id-Version: meta-tag-manager 0.5.2
Report-Msgid-Bugs-To: http://wordpress.org/tag/meta-tag-manager
POT-Creation-Date: 2009-09-21 15:06+0000
PO-Revision-Date: 2016-07-07 16:32+0200
Last-Translator: Marcus Sykes <marcus@netweblogic.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl_NL
X-Generator: Poedit 1.8.8
 Плагин для управления мета-тегами. Добавить новый тег Введите название ссылки (что-то что поможет запомнить мета-тег) и потом введите необходимые значения для мета-тега. Только для главной страницы Мета-тег Менеджер мета-тегов Название ссылки Удалить 