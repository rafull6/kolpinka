<?php
/*
Plugin Name: Kolpinka Data Widget
Description: Отображение информации о лесной продукции в правом сайдбаре
Plugin URI: http://kolpinka.ru/
Version: 1.0
Author: Vlad Udovychenko
Author URI: http://vladysdovich.com/
*/

class DataDisplayer extends WP_Widget {
     public function __construct() {
           parent::__construct(
                 'DataDisplayer',
                 'Сумма и объем',
                 array( 'description' => __( 'Виджет отображения данных о лесных товарах', 'text_domain' ), )
           );
     }
     public function update( $new_instance, $old_instance ) {
           $instance = array();
           $instance['title'] = strip_tags( $new_instance['title'] );
           return $instance;
     }
     public function widget( $args, $instance ) {
           $value1 = get_field( "sum" );
           $value2 = get_field( "min-cap" );
		   $clear = '<div style="clear: both;"></div>';
		   
		   echo '<div class="full-half">';
           if (!empty($value1)){
	           echo '<div class="half"><p>Сумма</p>';
	           echo '<div class="half-input">';
	           echo $value1;
	           echo '</div></div>';
           }else{
           	echo '';
           }
           if (!empty($value2)){
	           echo '<div class="half"><p>Мин. объём</p>';
	           echo '<div class="half-input">';
	           echo $value2;
	           echo '</div></div>';
	       }else{
	       		echo '';
	       }
		   echo '</div>' . $clear;
		   
     }
}
add_action( 'widgets_init', function(){
     register_widget( 'DataDisplayer' );
});

?>