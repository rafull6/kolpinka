<?php
/**
 * Plugin Name: TinyMCE Button
 * Version:     0.0.1
 * Author:      Vladyslav Udovychenko
 * Author URI:  http://vladysdovich.com/
 * License:     GPLv2
 */

/* Plugin Name: My TinyMCE Buttons */
add_action( 'admin_init', 'my_tinymce_button' );

function my_tinymce_button() {
     if ( current_user_can( 'edit_posts' ) && current_user_can( 'edit_pages' ) ) {
          add_filter( 'mce_buttons', 'my_register_tinymce_button' );
          add_filter( 'mce_external_plugins', 'my_add_tinymce_button' );
     }
}

function my_register_tinymce_button( $buttons ) {
     array_push( $buttons, "button_eek", "margin_button" );
     return $buttons;
}

function my_add_tinymce_button( $plugin_array ) {
     $plugin_array['my_button_script'] = plugins_url( '/mybuttons.js', __FILE__ ) ;
     return $plugin_array;
}