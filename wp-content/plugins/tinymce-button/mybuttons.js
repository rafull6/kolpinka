(function() {
     tinymce.create('tinymce.plugins.MyButtons', {
          init : function(ed, url) {
               ed.addButton( 'margin_button', {
                    title : 'Добавить отступ',
                    image : '../wp-includes/images/margin.png',
                    cmd: 'margin_button_cmd'
               });
               ed.addCommand( 'margin_button_cmd', function() {
                    var return_text = '';
                    return_text = '<p></p>';
                    ed.execCommand('mceInsertContent', 0, return_text);
               });
          },
          createControl : function(n, cm) {
               return null;
          },
     });
     tinymce.PluginManager.add( 'my_button_script', tinymce.plugins.MyButtons );
})();